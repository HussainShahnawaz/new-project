package seleniumFirst;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;

public class Add_to_cart {

    public static void main(String s[]) {
        WebDriver driver;
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        driver.get("http://automationpractice.com/index.php");


        driver.findElement(By.linkText("Sign in")).click();
        driver.findElement(By.id("email")).sendKeys("hussainnn@gmail.com");
        driver.findElement(By.id("passwd")).sendKeys("123456@");
        driver.findElement(By.id("SubmitLogin")).click();
        driver.findElement(By.linkText("WOMEN")).click();
        driver.findElement(By.linkText("Tops")).click();
        driver.findElement(By.linkText("T-shirts")).click();
        driver.findElement(By.id("list")).click();

        WebElement MoreBtn = driver.findElement(By.xpath("//*[@id=\"center_column\"]/ul/li/div/div/div[3]/div/div[2]/a[2]"));
        Actions actions = new Actions(driver);
        actions.moveToElement(MoreBtn).click().perform();
        driver.findElement(By.id("quantity_wanted")).clear();
        driver.findElement(By.id("quantity_wanted")).sendKeys("2");
        WebElement Sizedrppdwn = driver.findElement(By.xpath("//*[@id='group_1']"));
        Select o = new Select(Sizedrppdwn);
        o.selectByVisibleText("M");
        driver.findElement(By.name("Submit")).click();
//        driver.findElement(By.xpath("//*[@id=\"add_to_cart\"]/button")).click();
//        driver.findElement(By.xpath("//*[@id=\"layer_cart\"]/div[1]/div[2]/div[4]/a")).click();
//        driver.findElement(By.xpath("//*[@id=\"center_column\"]/p[2]/a[1]")).click();
//        driver.findElement(By.xpath("//*[@id=\"center_column\"]/form/p/button")).click();
//        driver.findElement(By.xpath("//*[@id=\"cgv\"]")).click();
//        driver.findElement(By.xpath("//*[@id=\"form\"]/p/button")).click();
//        driver.findElement(By.xpath("//*[@id=\"HOOK_PAYMENT\"]/div[2]/div/p/a")).click();
//        driver.findElement(By.xpath("//*[@id=\"cart_navigation\"]/button")).click();
        driver.quit();
    }
}
