package POM;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;

public class DashbBoar extends pageInit {

    public WebDriver driver;
    public DashbBoar(WebDriver driver) {
        super(driver);
        this.driver=driver;
    }

    @FindBy(linkText = "Women")
    WebElement women;

    @FindBy(xpath="//*[@id=\"center_column\"]/ul/li[2]/div/div[1]/div/a[1]/img")
    WebElement image;


    @FindBy(xpath="//*[@id=\"center_column\"]/ul/li[2]/div/div[2]/div[2]/a[2]\n")
    WebElement morebtn;

    public void woman(){
        women.click();
    }

    public void morebtnclk(){
        Actions ac = new Actions(driver);
        ac.moveToElement(image).moveToElement(morebtn).click().perform();

    }


}
