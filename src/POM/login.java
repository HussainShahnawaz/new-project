package POM;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.util.Properties;

public class login{

        public static WebDriver driver;

        public static void main(String[] args) throws IOException, ParseException, org.json.simple.parser.ParseException {
            FileInputStream fileInput = new FileInputStream("/Users/shahnawaz/IdeaProjects/assignmtnts/src/POM/property/data.Properties");
            Properties prop = new Properties();
            prop.load(fileInput);

            if(prop.getProperty("browser").equals("chrome")){
                WebDriverManager.chromedriver().setup();
                driver = new ChromeDriver();


            }
            else {
                WebDriverManager.firefoxdriver().setup();
                driver = new FirefoxDriver();


            }

            JSONParser p= new JSONParser();
            Object obj=p.parse(new FileReader("/Users/shahnawaz/IdeaProjects/assignmtnts/src/POM/datasignup/signup_data.json"));
            JSONObject jObj =  (JSONObject)obj;
            JSONArray array=(JSONArray)jObj.get("SignUpData");

            JSONObject userDetail = (JSONObject) array.get(0);
            driver.get(prop.getProperty("url"));
            HomePage hp = new HomePage(driver);
            hp.signIn();

            Signup su = new  Signup(driver);

            su.EnterEmail2((String) userDetail.get("email_create"));
            su.EnterPassword((String) userDetail.get("passwd"));
            su.SubmitlogIn();
            DashbBoar d = new DashbBoar(driver);
            d.woman();
            d.morebtnclk();
            Add_to_cart ac = new Add_to_cart(driver);
            ac.qnt();
            ac.sze();
            ac.add();


        }
}

