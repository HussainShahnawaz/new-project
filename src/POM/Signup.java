package POM;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class Signup extends pageInit {

    public Signup(WebDriver driver) {
        super(driver);

    }

    @FindBy(id="email_create")
    WebElement emails1;

    @FindBy(id="SubmitCreate")
    WebElement Submitbuttonsign;

    @FindBy(id="email")
    WebElement emails2;

    @FindBy(id="passwd")
    WebElement password;

    @FindBy(id="SubmitLogin")
    WebElement submitbuttonlog;


    public void enteremail(String email){

        emails1.sendKeys(email);

    }


    public void SubmitCreate(){
        Submitbuttonsign.click();
    }


    public void EnterEmail2(String email){
        emails2.sendKeys(email);
    }

    public void EnterPassword(String passwd){
        password.sendKeys(passwd);
    }

    public void SubmitlogIn() {
        submitbuttonlog.click();
    }




}

