package POM;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;

public class Add_to_cart extends pageInit {


    public Add_to_cart(WebDriver driver) {
        super(driver);
    }

    @FindBy(xpath="//*[@id=\"quantity_wanted_p\"]/a[2]")
    WebElement quantity;

    @FindBy(id = "group_1")
    WebElement size;

    @FindBy(xpath ="//*[@id=\"add_to_cart\"]/button")
    WebElement addtocart;



    public void qnt()
    {
        quantity.click();

    }
    public void sze(){
        Select dropdown3 = new Select(size);
        dropdown3.selectByVisibleText("M");
    }
    public void add()
    {
        addtocart.click();
    }
}
