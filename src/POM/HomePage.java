package POM;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class HomePage extends pageInit{



    public HomePage(WebDriver driver){

        super(driver);
    }

    @FindBy(linkText = "Sign in")
    WebElement sign;

    public void signIn(){

        sign.click();
    }

}

